//
//  GamesTableViewController.swift
//  APIDemo
//
//  Created by MacStudent on 2019-07-02.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import WatchConnectivity
import SwiftyJSON

class GamesTableViewController: UITableViewController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
    
    }
    

    var gameList:[Game] = []
    var gameSubList:[Game] = []
    var jsonResponse:JSON!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("------------------------------------")
        //print("\(jsonResponse.dictionary)")
        for (index, e) in self.gameList.enumerated() {
            print("\(e.Coordinates)")
            print("\(e.GameDate)")
            print("\(e.GameTime)")
            print("\(e.GameSection)")
            print("\(e.Location)")
            print("\(e.TeamA)")
            print("\(e.TeamB)")
        }
        print("------------------------------------")
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        if (WCSession.isSupported()) {
            print("PHONE: Phone supports WatchConnectivity!")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            print("PHONE: Phone does not support WatchConnectivity")
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return gameList.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! GamesTableViewCell

        // Configure the cell...
        
        
        
        
        cell.teamAImg.image = UIImage(named: gameList[indexPath.row].TeamA)
        cell.teamBImg.image = UIImage(named: gameList[indexPath.row].TeamB)
        cell.descLbl.text = "\(gameList[indexPath.row].GameSection!)"
        cell.desc2Lbl.text = "Vs"
        cell.desc3Lbl.text = "\(gameList[indexPath.row].GameDate!)"
        cell.desc4Lbl.text = "\(gameList[indexPath.row].GameTime!)"
        cell.desc5Lbl.text = "Not Subscribed"
        cell.desc5Lbl.textColor = UIColor.red
        //cell.contentView.backgroundColor = UIColor.gray
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let i = indexPath.row

        print("Person clicked in row number: \(i)")
        print("\(gameList[indexPath.row].TeamA!) Vs \(gameList[indexPath.row].TeamB!)")
        
        
        if (gameList[indexPath.row].Subscribed!) {
            //var selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath as IndexPath)!
            //selectedCell.contentView.backgroundColor = UIColor.gray
            //cell.desc5Lbl.text = "Not Subscribed"
            //cell.desc5Lbl.textColor = UIColor.red
            let cell = tableView.cellForRow(at: indexPath) as! GamesTableViewCell
            cell.desc5Lbl.text = "Not Subscribed"
            cell.desc5Lbl.textColor = UIColor.red
            
        }else{
            //var selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath as IndexPath)!
            //selectedCell.contentView.backgroundColor = UIColor.red
            let cell = tableView.cellForRow(at: indexPath) as! GamesTableViewCell
            cell.desc5Lbl.text = "Subscribed"
            cell.desc5Lbl.textColor = UIColor.green
        }
        gameList[indexPath.row].Subscribed = !gameList[indexPath.row].Subscribed!
        
        print(jsonResponse)
        
        //var json:String = "{\"id\": 24, \"name\": \"Bob Jefferson\", \"friends\": [{\"id\": 29, \"name\": \"Jen Jackson\"}]}"
        var json:String = "["
        //var listToSend:String = "["
        
        for (index, e) in self.gameList.enumerated() {
            
            json += "{\"gameSection\": \"\(e.GameSection!)\",\"gameDate\": \"\(e.GameDate!)\",\"gameTime\": \"\(e.GameTime!)\",\"location\": \"\(e.Location!)\",\"lat\": \"\(e.Coordinates.latitude)\",\"lon\": \"\(e.Coordinates.longitude)\",\"teamA\": \"\(e.TeamA!)\",\"teamB\": \"\(e.TeamB!)\",\"subscribed\": \"\(e.Subscribed!)\"}"
            //listToSend.append(json)
            //listToSend[index] = json
            if(index < self.gameList.count-1){
                json += ","
            }
            
        }
        json += "]"
        /*
         private var gameSection : String!
         private var gameDate : String!
         private var gameTime : String!
         private var location : String!
         private var coordinates:CLLocationCoordinate2D!
         private var teamA : String!
         private var teamB : String!
         private var subscribed : Bool!
         */
        
        print("************")
        
        sendDataToWatch(listToSend: json)
    }
    
    func sendDataToWatch(listToSend : String) {
        
        print("daasda")
        let abc = ["gameList": listToSend]
        //let abc = listToSend
        if (WCSession.default.isReachable) {
            
            WCSession.default.sendMessage(abc, replyHandler: nil)
        }
        else {
            print("PHONE: Cannot find the watch")
        }
        
    }

    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }*/
    

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

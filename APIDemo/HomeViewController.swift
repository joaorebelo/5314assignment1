//
//  Home.swift
//  APIDemo
//
//  Created by MacStudent on 2019-07-02.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import FirebaseFirestore
//import CoreLocation
import MapKit
import WatchConnectivity


class HomeViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
    }
    var annotation:MKAnnotation!
    var manager:CLLocationManager!
    var db:Firestore!
    var gameList:[Game] = []
    var jsonResponse: JSON!
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBAction func gamesListBtn(_ sender: UIButton) {
        var json:String = "["
        //var listToSend:String = "["
        
        for (index, e) in self.gameList.enumerated() {
            
            json += "{\"gameSection\": \"\(e.GameSection!)\",\"gameDate\": \"\(e.GameDate!)\",\"gameTime\": \"\(e.GameTime!)\",\"location\": \"\(e.Location!)\",\"lat\": \"\(e.Coordinates.latitude)\",\"lon\": \"\(e.Coordinates.longitude)\",\"teamA\": \"\(e.TeamA!)\",\"teamB\": \"\(e.TeamB!)\",\"subscribed\": \"\(e.Subscribed!)\"}"
            if(index < self.gameList.count-1){
                json += ","
            }
            
        }
        json += "]"
        
        print("************")
        
        sendDataToWatch(listToSend: json)
    }
    
    func sendDataToWatch(listToSend : String) {
        
        print("daasda")
        let abc = ["gameList": listToSend]
        //let abc = listToSend
        if (WCSession.default.isReachable) {
            
            WCSession.default.sendMessage(abc, replyHandler: nil)
        }
        else {
            print("PHONE: Cannot find the watch")
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (WCSession.isSupported()) {
            print("PHONE: Phone supports WatchConnectivity!")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            print("PHONE: Phone does not support WatchConnectivity")
        }
        self.mapView.delegate = self
        
        
        let URL = "https://examplefirebase-d03e3.firebaseio.com/fifa2019.json"
        //
        Alamofire.request(URL).responseJSON {
            response in
            
            guard let apiData = response.result.value else {
                print("Error getting data from the URL")
                return
            }
            
            self.jsonResponse = JSON(apiData)
            
            var aux = self.jsonResponse["Semi-final"][0].dictionary
            self.gameList.append(Game(gameSection: "Semi-final", gameDate: aux!["gameDate"]!.string!, gameTime: aux!["gameTime"]!.string!, location: aux!["location"]!.string!, coordinates: CLLocationCoordinate2D(latitude: CLLocationDegrees(aux!["lat"]!.float!), longitude: CLLocationDegrees(aux!["lon"]!.float!)), teamA: aux!["teamA"]!.string!, teamB: aux!["TeamB"]!.string!))
            aux = self.jsonResponse["Semi-final"][1].dictionary
            self.gameList.append(Game(gameSection: "Semi-final", gameDate: aux!["gameDate"]!.string!, gameTime: aux!["gameTime"]!.string!, location: aux!["location"]!.string!, coordinates: CLLocationCoordinate2D(latitude: CLLocationDegrees(aux!["lat"]!.float!), longitude: CLLocationDegrees(aux!["lon"]!.float!)), teamA: aux!["teamA"]!.string!, teamB: aux!["TeamB"]!.string!))
            
            aux = self.jsonResponse["Final"][0].dictionary
            self.gameList.append(Game(gameSection: "Final", gameDate: aux!["gameDate"]!.string!, gameTime: aux!["gameTime"]!.string!, location: aux!["location"]!.string!, coordinates: CLLocationCoordinate2D(latitude: CLLocationDegrees(aux!["lat"]!.float!), longitude: CLLocationDegrees(aux!["lon"]!.float!)), teamA: aux!["teamA"]!.string!, teamB: aux!["TeamB"]!.string!))
            
            aux = self.jsonResponse["MFTPlace"][0].dictionary
            self.gameList.append(Game(gameSection: "Final", gameDate: aux!["gameDate"]!.string!, gameTime: aux!["gameTime"]!.string!, location: aux!["location"]!.string!, coordinates: CLLocationCoordinate2D(latitude: CLLocationDegrees(aux!["lat"]!.float!), longitude: CLLocationDegrees(aux!["lon"]!.float!)), teamA: aux!["teamA"]!.string!, teamB: aux!["TeamB"]!.string!))
            
            aux = self.jsonResponse["Quarter-final"][0].dictionary
            self.gameList.append(Game(gameSection: "Quarter-final", gameDate: aux!["gameDate"]!.string!, gameTime: aux!["gameTime"]!.string!, location: aux!["location"]!.string!, coordinates: CLLocationCoordinate2D(latitude: CLLocationDegrees(aux!["lat"]!.float!), longitude: CLLocationDegrees(aux!["lon"]!.float!)), teamA: aux!["teamA"]!.string!, teamB: aux!["TeamB"]!.string!))
            aux = self.jsonResponse["Quarter-final"][1].dictionary
            self.gameList.append(Game(gameSection: "Quarter-final", gameDate: aux!["gameDate"]!.string!, gameTime: aux!["gameTime"]!.string!, location: aux!["location"]!.string!, coordinates: CLLocationCoordinate2D(latitude: CLLocationDegrees(aux!["lat"]!.float!), longitude: CLLocationDegrees(aux!["lon"]!.float!)), teamA: aux!["teamA"]!.string!, teamB: aux!["TeamB"]!.string!))
            aux = self.jsonResponse["Quarter-final"][2].dictionary
            self.gameList.append(Game(gameSection: "Quarter-final", gameDate: aux!["gameDate"]!.string!, gameTime: aux!["gameTime"]!.string!, location: aux!["location"]!.string!, coordinates: CLLocationCoordinate2D(latitude: CLLocationDegrees(aux!["lat"]!.float!), longitude: CLLocationDegrees(aux!["lon"]!.float!)), teamA: aux!["teamA"]!.string!, teamB: aux!["TeamB"]!.string!))
            aux = self.jsonResponse["Quarter-final"][3].dictionary
            self.gameList.append(Game(gameSection: "Quarter-final", gameDate: aux!["gameDate"]!.string!, gameTime: aux!["gameTime"]!.string!, location: aux!["location"]!.string!, coordinates: CLLocationCoordinate2D(latitude: CLLocationDegrees(aux!["lat"]!.float!), longitude: CLLocationDegrees(aux!["lon"]!.float!)), teamA: aux!["teamA"]!.string!, teamB: aux!["TeamB"]!.string!))
            
            
            
            var annotations = [MKPointAnnotation]()
            for (_, game) in self.gameList.enumerated() {
                let annotation = MKPointAnnotation()
                annotation.coordinate = game.Coordinates!
                annotation.title = "\(game.TeamA!) vs \(game.TeamB!)"
                annotation.subtitle = "\(game.Location!)"
                annotations.append(annotation)
            }
            self.mapView.addAnnotations(annotations)
            
            let x = CLLocationCoordinate2DMake(48.2061353, 0.5475205)
            let y = MKCoordinateSpan(latitudeDelta: 18, longitudeDelta: 18)
            let z = MKCoordinateRegion(center: x, span: y)
            self.mapView.setRegion(z, animated: true)
            
            self.reorderGamelist()
        }
    }
    
    func reorderGamelist(){
    
        self.gameList = gameList.sorted(by: {
            let splitString = $0.GameDate!.components(separatedBy: " ");
            let splitString2 = $1.GameDate!.components(separatedBy: " ");
            var m0 = 0
            var m1 = 0
            if(splitString[1] == "Jun"){m0 = 6}else{m0 = 7}
            if(splitString2[1] == "Jun"){m1 = 6}else{m1 = 7}
            
        
            
            let date0 = "" + splitString[2] + "\(m0)" + splitString[0] + $0.GameTime
            let date1 = "" + splitString2[2] + "\(m1)" + splitString2[0] + $1.GameTime

            return (date0 < date1)
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let n1 = segue.destination as! GamesTableViewController
        n1.gameList = self.gameList
        n1.jsonResponse = self.jsonResponse
        
    }
    
    
}

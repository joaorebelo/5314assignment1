//
//  SubscribedInterfaceController.swift
//  APIDemo WatchKit Extension
//
//  Created by Joao Rebelo on 2019-07-02.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import SwiftyJSON
import WatchConnectivity

class SubscribedInterfaceController: WKInterfaceController , WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
    
    var gameList:[Game] = []
    
    @IBOutlet var gamesSubbedLbl: WKInterfaceLabel!
    @IBOutlet var SubbedGamesTable: WKInterfaceTable!
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        let gamesList2 = message["gameList"] as! String
        
        if let data = gamesList2.data(using: .utf8) {
            if let json = try? JSON(data: data) {
                InterfaceController.gameList = []
                for item in json.arrayValue {
                    var game = item.dictionary!
                    InterfaceController.gameList.append(Game(gameSection: game["gameSection"]!.string!, gameDate: game["gameDate"]!.string!, gameTime: game["gameTime"]!.string!, location: game["location"]!.string!, coordinates: CLLocationCoordinate2D(latitude: 43.653, longitude: -79.283), teamA: game["teamA"]!.string!, teamB: game["teamB"]!.string!, subscribed: (game["subscribed"]! == "true") ? true : false))
                }
            }
        }
        
        reloadTable()
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        
        if (WCSession.isSupported()) {
            print("WATCH: WCSession is supported!")
            let session = WCSession.default
            session.delegate = self as! WCSessionDelegate
            session.activate()
        }
        else {
            print("WATCH: Does not support WCSession, sorry!")
        }
        
        reloadTable()
    }
    
    func reloadTable() {
        self.gameList = []
        for (index, x) in InterfaceController.gameList.enumerated() {
            print("\(index), \(x)")
            if (x.Subscribed){
                gameList.append(x)
            }
        }
        //set total rows number
        self.SubbedGamesTable.setNumberOfRows(
            self.gameList.count, withRowType:"myRow2"
        )
        
        if (gameList.count > 0) {
            //fill table
            for (index, x) in self.gameList.enumerated() {
                let row = self.SubbedGamesTable.rowController(at: index) as! SubcribedGamesRowController
                row.teamAImg.setImage(UIImage(named: gameList[index].TeamA))
                row.teamBImg.setImage(UIImage(named: gameList[index].TeamB))
                row.vsLbl.setText("\(gameList[index].GameTime!)\n\(gameList[index].GameDate!)")
            }
            self.gamesSubbedLbl.setText("Games Subbed")
        }else{
            self.gamesSubbedLbl.setText("No Games")
        }
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}

//
//  ScheduleGamesRowController.swift
//  APIDemo WatchKit Extension
//
//  Created by Joao Rebelo on 2019-07-05.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit

class ScheduleGamesRowController: NSObject {

    @IBOutlet var teamAImg: WKInterfaceImage!
    @IBOutlet var teamBImg: WKInterfaceImage!
    @IBOutlet var vsLbl: WKInterfaceLabel!
}

//
//  ScheduleInterfaceController.swift
//  APIDemo WatchKit Extension
//
//  Created by Joao Rebelo on 2019-07-02.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import SwiftyJSON
import WatchConnectivity

class ScheduleInterfaceController: WKInterfaceController, WCSessionDelegate {
    var gameList:[Game] = []
    
    //outlets
    @IBOutlet var scheduleGamesTable: WKInterfaceTable!
    
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        let gamesList2 = message["gameList"] as! String
        
        if let data = gamesList2.data(using: .utf8) {
            if let json = try? JSON(data: data) {
                InterfaceController.gameList = []
                for item in json.arrayValue {
                    var game = item.dictionary!
                    InterfaceController.gameList.append(Game(gameSection: game["gameSection"]!.string!, gameDate: game["gameDate"]!.string!, gameTime: game["gameTime"]!.string!, location: game["location"]!.string!, coordinates: CLLocationCoordinate2D(latitude: 43.653, longitude: -79.283), teamA: game["teamA"]!.string!, teamB: game["teamB"]!.string!, subscribed: (game["subscribed"]! == "true") ? true : false))
                }
            }
        }
        
        reloadTable()
        
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        if (WCSession.isSupported()) {
            print("WATCH: WCSession is supported!")
            let session = WCSession.default
            session.delegate = self as! WCSessionDelegate
            session.activate()
        }
        else {
            print("WATCH: Does not support WCSession, sorry!")
        }
        
        
        reloadTable()
        /*
        print("------------------------------------")
        //print("\(jsonResponse.dictionary)")
        for (index, e) in self.gameList.enumerated() {
            print("\(e.Coordinates)")
            print("\(e.GameDate)")
            print("\(e.GameTime)")
            print("\(e.GameSection)")
            print("\(e.Location)")
            print("\(e.TeamA)")
            print("\(e.TeamB)")
        }
        print("------------------------------------")*/
    }
    
    func reloadTable() {
        self.gameList = InterfaceController.gameList
        //set total rows number
        self.scheduleGamesTable.setNumberOfRows(
            self.gameList.count, withRowType:"myRow"
        )
        //fill table
        for (index, x) in self.gameList.enumerated() {
            print("\(index), \(x)")
            let row = self.scheduleGamesTable.rowController(at: index) as! ScheduleGamesRowController
            row.teamAImg.setImage(UIImage(named: gameList[index].TeamA))
            row.teamBImg.setImage(UIImage(named: gameList[index].TeamB))
            row.vsLbl.setText("\(gameList[index].GameTime!)\n\(gameList[index].GameDate!)")
            
        }
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}

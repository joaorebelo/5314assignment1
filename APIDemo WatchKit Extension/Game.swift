//
//  Game.swift
//  APIDemo WatchKit Extension
//
//  Created by Joao Rebelo on 2019-07-02.
//  Copyright © 2019 Parrot. All rights reserved.
//

import Foundation
import SpriteKit
import CoreLocation

class Game{
    private var gameSection : String!
    private var gameDate : String!
    private var gameTime : String!
    private var location : String!
    private var coordinates:CLLocationCoordinate2D!
    private var teamA : String!
    private var teamB : String!
    private var subscribed : Bool!
    
    
    
    var GameSection : String!{
        get{return self.gameSection}
        set{self.gameSection = newValue}
    }
    
    var GameDate : String!{
        get{return self.gameDate}
        set{self.gameDate = newValue}
    }
    
    var GameTime : String!{
        get{return self.gameTime}
        set{self.gameTime = newValue}
    }
    
    var Location : String!{
        get{return self.location}
        set{self.location = newValue}
    }
    
    var Coordinates : CLLocationCoordinate2D!{
        get{return self.coordinates}
        set{self.coordinates = newValue}
    }
    
    var TeamA : String!{
        get{return self.teamA}
        set{self.teamA = newValue}
    }
    
    var TeamB : String!{
        get{return self.teamB}
        set{self.teamB = newValue}
    }
    
    var Subscribed : Bool!{
        get{return self.subscribed}
        set{self.subscribed = newValue}
    }
    
    init(gameSection: String, gameDate: String, gameTime: String, location: String, coordinates: CLLocationCoordinate2D, teamA: String, teamB: String) {
        self.gameSection = gameSection
        self.gameDate = gameDate
        self.gameTime = gameTime
        self.location = location
        self.coordinates = coordinates
        self.teamA = teamA
        self.teamB = teamB
        self.subscribed = false
    }
    
    init(gameSection: String, gameDate: String, gameTime: String, location: String, coordinates: CLLocationCoordinate2D, teamA: String, teamB: String, subscribed: Bool) {
        self.gameSection = gameSection
        self.gameDate = gameDate
        self.gameTime = gameTime
        self.location = location
        self.coordinates = coordinates
        self.teamA = teamA
        self.teamB = teamB
        self.subscribed = subscribed
    }
    
    
}

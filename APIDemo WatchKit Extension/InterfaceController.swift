//
//  InterfaceController.swift
//  APIDemo WatchKit Extension
//
//  Created by Parrot on 2019-03-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {
    static var gameList:[Game] = []
    //var currentvalue:String = "teste22"
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        //print(message)
        //print("------")
        //print(message["gameList"])

        let gamesList2 = message["gameList"] as! String
        if let data = gamesList2.data(using: .utf8) {
            if let json = try? JSON(data: data) {
                //print(json)
                InterfaceController.gameList = []
                for item in json.arrayValue {
                    var game = item.dictionary!
                    print("\(game["gameSection"]!.string!)")
                    print("\(game["gameDate"]!.string!)")
                    print("\(game["gameTime"]!.string!)")
                    print("\(game["location"]!.string!)")
                    print("\(game["teamA"]!.string!)")
                    print("\(game["teamB"]!.string!)")
                    print("\(game["gameSection"]!.string!)")
                    print("\(game["gameSection"]!.string!)")
                    print("\(game["gameSection"]!.string!)")
                    
                    InterfaceController.gameList.append(Game(gameSection: game["gameSection"]!.string!, gameDate: game["gameDate"]!.string!, gameTime: game["gameTime"]!.string!, location: game["location"]!.string!, coordinates: CLLocationCoordinate2D(latitude: 43.653, longitude: -79.283), teamA: game["teamA"]!.string!, teamB: game["teamB"]!.string!, subscribed: (game["subscribed"]! == "true") ? true : false))
                }
            }
        }
    }
    
    // MARK: Outlet
    // ---------------
 
    
    // MARK: Actions
    @IBAction func getDataPressed() {
        print("Watch button pressed")
        
        /*
        let n1 = semun.destination as! ScheduleInterfaceController
        n1.gameList = self.gameList
        */
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        if (WCSession.isSupported()) {
            print("WATCH: WCSession is supported!")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            print("WATCH: Does not support WCSession, sorry!")
        }
        
    }
   
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    /*
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let n1 = segue.destination as! GamesTableViewController
        n1.gameList = self.gameList
        n1.jsonResponse = self.jsonResponse
        
    }*/
/*
    func contextForSegueWithIdentifier(segueIdentifier: String) -> AnyObject? {
        // You may want to set the context's identifier in Interface Builder and check it here to make sure you're returning data at the proper times
        
        // Return data to be accessed in ResultsController
        return self.currentvalue as AnyObject
    }*/

}
